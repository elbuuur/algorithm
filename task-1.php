<?php
session_start();

$s = (int)$_POST['s'];

if($s <= 1) {
    $_SESSION['error'] = 's>1!';
    header('Location: /index-1.php');
}

$sum = 1;
$allSum = $i = 0;

while ($i < $s) {
    $i++;
    $fraction = (($i - 1) * 3 + 1) / $i;
    $allSum+=$fraction;

    if($allSum > $s) {
        $_SESSION['success'] = 'Нужно сложить '.$i.' элементов, чтобы получить '.$s;
        header('Location: /index-1.php');
        break;
    }
}
