<?php session_start()
/*
 * блок 1, задание 5
 *  Дана последовательность, состоящая из дробей: 1/1, 4/2, 7/3, 10/4,... Какое минимальное количество элементов последовательности нужно сложить, чтобы сумма превысила заданное число S > 1?
 */
?>

<?php if($_SESSION['error']): ?>
    <p><?= $_SESSION['error']; unset($_SESSION['error'])?></p>
<?php endif; ?>

<form action="task-1.php" method="post">
    <input name="s" placeholder="Введите значение s:">
    <button type="submit">Обработать</button>
</form>

<?php if($_SESSION['success']): ?>
    <p><?= $_SESSION['success']; unset($_SESSION['success'])?></p>
<?php endif; ?>